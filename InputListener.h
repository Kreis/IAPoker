#ifndef _INPUTLISTENER_H_
#define _INPUTLISTENER_H_

#include <memory>
#include <functional>
#include <ctime>
#include "Status.h"

enum RESPONSE {
	LOW 		= 1,
	MIDDLE 		= 2,
	HIGH 		= 3
};

class InputListener {
public:
	virtual void take_decision(Status& status, RESPONSE& out_response, double& out_value, int id_asigned) = 0;
};

#endif