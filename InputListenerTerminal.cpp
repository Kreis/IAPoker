#include "InputListenerTerminal.h"

InputListenerTerminal::InputListenerTerminal() {

};

void InputListenerTerminal::take_decision(Status& status, RESPONSE& out_response, double& out_value, int id_asigned) {

	std::function<std::string(int)> get_type = [](int v) {
		switch (v) {
			case 1: return "Spade";
			case 2: return "Diamond";
			case 3: return "Clover";
			case 4: return "Heart";
		}
		return "-";
	};

	std::cout << "current player: " << id_asigned << std::endl;
	std::cout << "current round: " << status.get_current_round() << std::endl;
	std::cout << "board amount: " << status.get_board_amount() << std::endl;
	std::cout << "number players: " << status.get_number_players() << std::endl;

	pCard card1;
	pCard card2;
	status.get_player(id_asigned)->get_cards(card1, card2);
	if (status.get_current_round() > 0) {
		std::cout << "card1: " << card1->number << " " << get_type(card1->type) << std::endl;
		std::cout << "card2: " << card2->number << " " << get_type(card2->type) << std::endl;
	}
	std::cout << "amount: " << status.get_player(id_asigned)->get_amount() << std::endl;

	std::vector<pCard>& board_cards = status.get_board_cards();
	if (board_cards.size() == 0)
		std::cout << "*No cards in board*" << std::endl;
	else for (int i = 0; i < board_cards.size(); i++)
		std::cout << "card in board: " << board_cards.at( i )->number << " " << get_type(board_cards.at( i )->type) << std::endl;

	std::cout << "**************************" << std::endl;

	std::cout << "what response?" << std::endl;
	int r;
	std::cin >> r;

	if (r == 0 || r == 1) {
		out_response = r == 0 ? RESPONSE::LOW : RESPONSE::MIDDLE;
	} else if (r == 2) {
		out_response = RESPONSE::HIGH;
		double v;
		std::cin >> v;
		out_value = v;
	}
}