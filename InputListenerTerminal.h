#ifndef _INPUTLISTENERTERMINAL_H_
#define _INPUTLISTENERTERMINAL_H_

#include <iostream>
#include "InputListener.h"

class InputListenerTerminal : public InputListener {
public:
	InputListenerTerminal();

	void take_decision(Status& status, RESPONSE& out_response, double& out_value, int id_asigned) override;
};

#endif