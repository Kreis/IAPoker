#include "Round.h"

Round::Round() {
	id_card = 0;
	for (int i = 2; i <= 14; i++)
		for (int j = 1; j <= 4; j++)
			deck.push_back(std::make_shared<Card>(i, j));
}

void Round::start() {
	Status status(10);

	shuffle_deck();

	status.add_player(std::make_shared<Player>(1000, draw_card(false), draw_card(false)));
	status.add_player(std::make_shared<Player>(1000, draw_card(false), draw_card(false)));

	status.put_button(0);

	new_round(status);
}

void Round::new_round(Status& status) {

	inputs.push_back(std::make_shared<InputListenerTerminal>());
	inputs.push_back(std::make_shared<InputListenerTerminal>());

	int id_player = status.get_id_button();
	status.required_amount = status.round_bet_amount;

	for (int i = 0; i < 5; i++) {

		int id_player = status.get_id_button();

		if (i == 0) {
			id_player = status.next_id_player(id_player);
			status.bet_amount(id_player, status.round_bet_amount / 2);

			id_player = status.next_id_player(id_player);
			status.bet_amount(id_player, status.round_bet_amount);
		}

		id_player = status.next_id_player(id_player);

		status.put_current_round(i);
		int id_turning_player = id_player;

		while (status.get_current_active_players() > 1) {
			pPlayer current_player = status.get_player(id_turning_player);

			RESPONSE r;
			double val;
			inputs.at( id_turning_player )->take_decision(status, r, val, id_turning_player);

			switch (r) {
				case RESPONSE::LOW:
					if (current_player->get_bet_amount() < status.required_amount)
						current_player->put_active(false);
				break;
				case RESPONSE::MIDDLE:
					if (current_player->get_bet_amount() < status.required_amount)
						status.bet_amount(id_turning_player, status.required_amount - current_player->get_bet_amount());
				break;
				case RESPONSE::HIGH:
					if (current_player->get_bet_amount() < status.required_amount)
						status.bet_amount(id_turning_player, status.required_amount - current_player->get_bet_amount());
					if (current_player->get_amount() > 0) {
						int to_upper_amount = (val > 0.95 ? 1.0 : val) * current_player->get_amount();
						status.bet_amount(id_turning_player, to_upper_amount);
						status.required_amount += to_upper_amount;
					}

					id_player = id_turning_player;
				break;
			}

			id_turning_player = status.next_id_player(id_turning_player);
			if (id_turning_player == id_player)
				break;
		}

		std::cout << "round: " << i << std::endl;

		if (status.get_current_active_players() <= 1)
			i = 4;

		if (i == 1) {
			status.add_board_card(draw_card(true));
			status.add_board_card(draw_card(true));
			status.add_board_card(draw_card(true));
		} else if (i == 2) {
			status.add_board_card(draw_card(true));
		} else if (i == 3) {
			status.add_board_card(draw_card(true));
		} else if (i == 4) {
			status.distribute_prizes();
		}
	}
}

pCard Round::draw_card(bool type_on_board) {
	id_card++;
	deck.at( id_card - 1 )->on_board = type_on_board;
	return deck.at( id_card - 1 );
}

int myrandom(int i) {
	return std::rand() % i;
}

void Round::shuffle_deck() {
	id_card = 0;
	std::srand(unsigned(std::time(0)));
	std::random_shuffle(deck.begin(), deck.end(), myrandom);
}

