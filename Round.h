#ifndef _ROUND_H_
#define _ROUND_H_

#include <iostream>
#include <memory>
#include "InputListenerTerminal.h"
#define pInput std::shared_ptr<InputListener>

class Round {
public:
	Round();

	void start();

private:
	int id_card;
	pCard draw_card(bool type_on_board);
	void shuffle_deck();

	void new_round(Status& status);

	std::vector<pInput> inputs;
	std::vector<pCard> deck;
};

#endif