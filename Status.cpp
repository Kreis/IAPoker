#include "Status.h"

Status::Status(int bet_amount) {
	board_amount = 0;
	round_bet_amount = bet_amount;
	required_amount = bet_amount;
	current_round = 0;
}

void Status::clean() {
	players.clear();
	board_cards.clear();
}

void Status::add_player(pPlayer player) {
	players.push_back(player);
}

void Status::add_board_card(pCard card) {
	board_cards.push_back(card);
}

void Status::put_button(int id_player) {
	id_button = id_player;
}

void Status::bet_amount(int id_player, int amount) {
	int bet_amount = players.at(id_player)->remove_amount(amount);
	board_amount += bet_amount;
}

void Status::win_amount(int id_player, int amount) {
	board_amount -= amount;
	players.at(id_player)->add_amount(amount);
}

pPlayer Status::get_player(int id_player) {
	return players.at(id_player);
}

int Status::get_board_amount() {
	return board_amount;
}

int Status::get_id_button() {
	return id_button;
}

int Status::get_number_players() {
	return players.size();
}

std::vector<pCard>& Status::get_board_cards() {
	return board_cards;
}

int Status::next_id_player(int id_player) {
	int id_next_player = id_player + 1 >= players.size() ? 0 : id_player + 1;
	while ( ! players.at(id_next_player)->get_active())
		id_next_player = id_next_player + 1 >= players.size() ? 0 : id_next_player + 1;

	return id_next_player;
}

void Status::put_current_round(int current_round) {
	this->current_round = current_round;
}

int Status::get_current_round() {
	return current_round;
}

void Status::distribute_prizes() {

	if (get_current_active_players() == 1) {
		std::cout << "just one" << std::endl;
		return;
	}

	std::vector<pCard> cards;
	for (pCard& card : board_cards)
		cards.push_back(card);

	std::vector<pHand> hands;
	int id_player = -1;
	for (int i = 0; i < players.size(); i++) if (players.at( i )->get_active()) {
		std::vector<pCard> current_cards = cards;

		pCard card1;
		pCard card2;
		players.at( i )->get_cards(card1, card2);
		current_cards.push_back(card1);
		current_cards.push_back(card2);
		
		hands.push_back(std::make_shared<Hand>(current_cards, i));
	}

	std::sort(hands.begin(), hands.end(), cmp_hand);

	std::vector<std::vector<pHand> > podium;
	int id_podium = -1;
	pHand current_hand = nullptr;
	for (pHand& hand : hands) {
		if ( ! eq_hand(hand, current_hand)) {
			std::vector<pHand> group_hands;
			podium.push_back(group_hands);
			id_podium++;
		}
		podium.at( id_podium ).push_back(hand);
	}

	for (std::vector<pHand>& vec : podium) {
		std::cout << "size group: " << vec.size() << std::endl;
	}

}

bool Status::cmp_hand(pHand& hand1, pHand& hand2) {
	if (hand1->get_val1() < hand2->get_val1())
		return true;
	if (hand1->get_val1() > hand2->get_val1())
		return false;

	std::vector<int>& hand1_val2 = hand1->get_val2();
	std::vector<int>& hand2_val2 = hand2->get_val2();
	for (int i = 0; i < hand1_val2.size(); i++)
		if (hand1_val2.at( i ) < hand2_val2.at( i ))
			return true;
		else if (hand1_val2.at( i ) > hand2_val2.at( i ))
			return false;

	return false;
}

bool Status::eq_hand(pHand& hand1, pHand& hand2) {
	if (hand1 == nullptr || hand2 == nullptr)
		return false;

	if (hand1->get_val1() < hand2->get_val1())
		return false;
	if (hand1->get_val1() > hand2->get_val1())
		return false;

	std::vector<int>& hand1_val2 = hand1->get_val2();
	std::vector<int>& hand2_val2 = hand2->get_val2();
	for (int i = 0; i < hand1_val2.size(); i++)
		if (hand1_val2.at( i ) < hand2_val2.at( i ))
			return false;
		else if (hand1_val2.at( i ) > hand2_val2.at( i ))
			return false;

	return true;
}