#ifndef _STATUS_H_
#define _STATUS_H_

#include <vector>
#include <iostream>
#include <memory>
#include <algorithm>
#define pCard std::shared_ptr<Card>
#define pPlayer std::shared_ptr<Player>
#define pHand std::shared_ptr<Hand>

class Card {
public:
	Card() {
		hide = true;
		on_board = false;
	}
	Card(int number, int type) {
		hide = false;
		this->number = number;
		this->type = type;
	}

	bool hide;
	int number;
	int type;
	bool on_board;
};

class Hand {
public:
	Hand(std::vector<pCard>& hand, int id_player) {
		this->id_player = id_player;
		value1 = -1;
		this->hand = hand;
	}

	int get_val1() {
		if (value1 == -1)
			determine_hand_value();
		return value1;
	}

	std::vector<int>& get_val2() {
		if (value2.size() == 0)
			determine_hand_value();
		return value2;
	}

private:
	int id_player;
	std::vector<pCard> hand;
	int value1;
	std::vector<int> value2;

	void determine_hand_value() {
		int ROYAL_FLUSH = 9;
		int POKER = 8;
		int FULL_HOUSE = 7;
		int FLUSH = 6;
		int STRAIGHT = 5;
		int THREE = 4;
		int TWO_PAIR = 3;
		int PAIR = 2;
		int HIGH_CARD = 1;

		std::sort(hand.begin(), hand.end(), Hand::cmp_number);

		std::vector<int> numbers(13, 0);
		int t_spaded = 0, t_diamond = 0, t_clover = 0, t_heart = 0;
		for (pCard& card : hand) {
			numbers[ card->number - 2 ]++;
			switch (card->type) {
				case 1: t_spaded++;
				break;
				case 2: t_diamond++;
				break;
				case 3: t_clover++;
				break;
				case 4: t_heart++;
				break;
			}
		}

		int id_flush = -1;
		if (t_spaded >= 5)
			id_flush = 1;
		if (t_diamond >= 5)
			id_flush = 2;
		if (t_clover >= 5)
			id_flush = 3;
		if (t_heart >= 5)
			id_flush = 4;

		int try_straight = -1, count_try_straight = 1, success_straight = -1;
		for (pCard& card : hand) if (card->type == id_flush) {

			if (card->number == try_straight + 1)
				count_try_straight++;
			else if (card->number != try_straight)
				count_try_straight = 1;

			try_straight = card->number;

			if (count_try_straight >= 5)
				success_straight = card->number;
		}

		if (id_flush > 0 && success_straight > 0) {
			value1 = ROYAL_FLUSH;
			value2.push_back(success_straight);
			return;
		}

		if (id_flush > 0) {
			value1 = FLUSH;
			value2.push_back(try_straight);
			return;
		}

		int try_full = -1;
		int pairs = -1;
		int two_pairs = -1;
		for (int i = numbers.size() - 1; i >= 0; i--) {
			int v = numbers.at( i );
			if (v == 4) {
				value1 = POKER;
				value1 = i + 2;
				return;
			}

			if (v == 3)
				try_full = i + 2;

			if (pairs > 0 && try_full > 0) {
				value1 = FULL_HOUSE;
				value2.push_back(try_full);
				return;
			}
			if (v == 2 && pairs > 0)
				two_pairs = i + 2;

			if (v == 2 && pairs == -1)
				pairs = i + 2;
		}

		try_straight = -1;
		count_try_straight = 1;
		success_straight = -1;
		for (pCard& card : hand) {
			if (card->number == try_straight + 1)
				count_try_straight++;
			else if (card->number != try_straight)
				count_try_straight = 1;
			
			try_straight = card->number;

			if (count_try_straight >= 5)
				success_straight = card->number;
		}

		if (success_straight > 0) {
			value1 = STRAIGHT;
			value2.push_back(success_straight);
			return;
		}

		if (try_full > 0) {
			value1 = THREE;
			value2.push_back(try_full);
			for (int i = numbers.size() - 1; i >= 0; i--) if (numbers[ i ] == 1 && value2.size() < 3)
				value2.push_back( i + 2 );
			return;
		}

		if (two_pairs > 0) {
			value1 = TWO_PAIR;
			value2.push_back(pairs);
			value2.push_back(two_pairs);
			for (int i = numbers.size() - 1; i >= 0; i--) if (numbers[ i ] == 1 && value2.size() < 3)
				value2.push_back( i + 2 );
			return;
		}

		if (pairs > 0) {
			value1 = PAIR;
			value2.push_back(pairs);
			for (int i = numbers.size() - 1; i >= 0; i--) if (numbers[ i ] == 1 && value2.size() < 4)
				value2.push_back( i + 2 );
			return;
		}

		value1 = HIGH_CARD;
		for (int i = numbers.size() - 1; i >= 0; i--) if (numbers[ i ] == 1 && value2.size() < 5)
				value2.push_back( i + 2 );
	}

	static bool cmp_number(pCard card1, pCard card2) {
		return card1->number < card2->number;
	}
};

class Player {
public:
	Player(int amount, pCard card1, pCard card2) {
		this->amount = amount;
		this->card1 = card1;
		this->card2 = card2;
		this->bet_amount = 0;
		active = true;
	}

	void get_cards(pCard& out_card1, pCard& out_card2) {
		out_card1 = this->card1;
		out_card2 = this->card2;
	}

	int get_amount() {
		return amount;
	}

	void set_amount(int amount) {
		this->amount = amount;
	}

	void add_amount(int amount) {
		this->amount += amount;
	}

	int remove_amount(int amount) {
		if (amount > this->amount)
			amount = this->amount;
		
		this->amount -= amount;
		bet_amount += amount;

		return amount;
	}

	int get_bet_amount() {
		return bet_amount;
	}

	void put_active(bool v) {
		active = v;
	}

	bool get_active() {
		return active;
	}

private:
	bool active;
	int amount;
	int bet_amount;
	pCard card1;
	pCard card2;
};

class Status {
public:
	Status(int bet_amount);

	int round_bet_amount;
	int required_amount;

	void clean();
	void add_player(pPlayer player);
	void add_board_card(pCard card);
	void put_button(int id_player);

	void bet_amount(int id_player, int amount);
	void win_amount(int id_player, int amount);

	pPlayer get_player(int id_player);

	int get_board_amount();
	int get_id_button();
	int get_number_players();

	std::vector<pCard>& get_board_cards();

	int next_id_player(int id_player);
	void put_current_round(int current_round);
	int get_current_round();

	int get_current_active_players() {
		int n = 0;
		for (pPlayer& player : players)
			n += player->get_active();
		return n;
	}

	void distribute_prizes();

private:
	int current_round;
	int id_button;
	int board_amount;
	
	std::vector<pPlayer> players;
	std::vector<pCard> board_cards;

	static bool cmp_hand(pHand& hand1, pHand& hand2);
	static bool eq_hand(pHand& hand1, pHand& hand2);
};

#endif