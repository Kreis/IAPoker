#include <iostream>
#include <memory>
#include <vector>
#include "Round.h"

int main(int argc, char** argv) {

	// std::vector<pCard> hand;
	// hand.push_back(std::make_shared<Card>(7, 3));
	// hand.push_back(std::make_shared<Card>(2, 2));
	// hand.push_back(std::make_shared<Card>(6, 4));
	// hand.push_back(std::make_shared<Card>(3, 4));
	// hand.push_back(std::make_shared<Card>(5, 3));
	// hand.push_back(std::make_shared<Card>(3, 1));
	// hand.push_back(std::make_shared<Card>(4, 1));

	// int val1 = -1;
	// std::vector<int> val2;
	// Hand::determine_hand_value(hand, val1, val2);

	// std::cout << "val1: " << val1 << std::endl;
	// for (int v : val2) {
	// 	std::cout << "val2: " << v << std::endl;
	// }

	// return 0;

	Round r;
	r.start();

	return 0;
}
